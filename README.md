# Small Netlify Site

[![Netlify Status](https://api.netlify.com/api/v1/badges/7b06e45e-f181-4e67-92b3-95ef74f8a2ae/deploy-status)](https://app.netlify.com/sites/loving-leavitt-fc24b6/deploys)

View live site here: [jaren.netlify.com](https://jaren.netlify.com/)


## Tools used during development phase:

- Adobe Illustrator, Photoshop, XD, After Effects, Media Encoder
- HTML, CSS, SASS, JavaScript
- Font Squirrel Webfont Generator [fontsquirrel.com/tools/webfont-generator](https://www.fontsquirrel.com/tools/webfont-generator)
- Zenscroll [zengabor.github.io/zenscroll](https://zengabor.github.io/zenscroll/)
- Netlify [netlify.com](https://www.netlify.com/)
